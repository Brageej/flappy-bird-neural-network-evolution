﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork
{
    public class BinaryConverter
    {
        public static string FloatArrayToBinaryString(float[] array)
        {
            StringBuilder sb = new StringBuilder();
            foreach(var value in array)
            {
                sb.Append(ToBinaryString(value));
            }
            return sb.ToString();
        }

        public static float[] BinaryArrayToFloatArray(string binaryArray)
        {
            float[] binaryFloats = new float[binaryArray.Length / 32];
            int index = 0;
            for (int i = 0; i < binaryFloats.Length; i++)
            {
                binaryFloats[i] = FromBinaryString(binaryArray.Substring(index, 32));
                index = index + 32;
            }
            return binaryFloats;
        }

        public static string ToBinaryString(float value)
        {
            return Convert.ToString(BitConverter.SingleToInt32Bits(value), 2).PadLeft(32, '0');
        }

        public static float FromBinaryString(string binary)
        {
            return BitConverter.Int32BitsToSingle(Convert.ToInt32(binary, 2));
        }
    }
}

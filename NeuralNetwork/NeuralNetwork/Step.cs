﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace NeuralNetwork
{
    public class Step : IActivationFunction
    {

        private double Threshold;
        public Step(double threshold)
        {
            Threshold = threshold;
        }
        public double[] CalculateOutput(double[] weigthedSums)
        {
            int length = weigthedSums.Length;
            double[] output = new double[length];
            for (int i = 0; i < length; i++)
            {
                //Debug.WriteLine(weigthedSums[i]);
                output[i] = weigthedSums[i] >= Threshold ? 1 : 0;
            }
            return output;
        }
    }
}

﻿using NeuralNetwork.Evolution;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork
{
    public static class Test
    {
        public static void Main(string[] args)
        {
            Properties props = new Properties
            {
                InputSize = 4,
                OutputSize = 1,
                NrHiddenLayers = 1,
                NrNeuronsInLayer = 3,
                HiddenLayerActivationFunction = new Sigmoid(),
                OutputLayerActivationFunction = new Step(0)
            };

            //var text = File.ReadAllText(@"c:\workspace\flappybirdneuralnetwork\results\best.txt").Split(';');
            //var weights = new double[text.Length + 3];
            //for (int i = 0; i < text.Length; i++)
            //{
            //    weights[i] = Convert.ToDouble(text[i]);
            //}
            var population = new Population(100, props);
            population.Individuals[0].Network.WeightsAsString();
            //if (weights.Length > 0)
            //{
            //    foreach (var individual in population.Individuals)
            //    {
            //        individual.Network.SetWeights(weights);
            //    }
            //}
            //for(int i = 0; i < population.Individuals.Length; i++)
            //{
            //    population.Individuals[i].Fitness = i;
            //}
            //Array.Sort(population.Individuals);
            //for (int i = 0; i < population.Individuals.Length; i++)
            //{
            //    Debug.WriteLine(population.Individuals[i].Fitness);
            //}
            //GenerationCreator generationCreator = new GenerationCreator();
            //var newGen = generationCreator.CreateGeneration(population.Individuals, 0.5, 0.5, 0.5);
            for (int i = 0; i < population.Individuals.Length; i++)
            {
                Debug.WriteLine(population.Individuals[i].Network.CalculateOutputs(new double[4] { 0.12037037037037, 0.24537037037037, 0.932316741943359, 0})[0]);
            }
        }
    }
}

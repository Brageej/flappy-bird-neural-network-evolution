﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace NeuralNetwork
{
    public class Network
    {
        public List<NetworkLayer> NetworkLayers { get; set; }

        public Properties Props { get; set; }

        //public double Fitness { get; set; }

        public Network(Properties props, Random random)
        {
            Props = props;
            CreateHiddenLayers(random);
        }

        public void CreateHiddenLayers(Random random)
        {
            NetworkLayers = new List<NetworkLayer>();
            for (int i = 0; i < Props.NrHiddenLayers; i++)
            {
                if (i == 0)
                {
                    NetworkLayers.Add(new NetworkLayer(Props.NrNeuronsInLayer, Props.InputSize, Props.HiddenLayerActivationFunction, false, random));
                }
                else
                {
                    NetworkLayers.Add(new NetworkLayer(Props.NrNeuronsInLayer, Props.NrNeuronsInLayer, Props.HiddenLayerActivationFunction, false, random));
                }
            }
            NetworkLayers.Add(new NetworkLayer(Props.OutputSize, Props.NrNeuronsInLayer, Props.OutputLayerActivationFunction, true, random));
        }

        public double[] CalculateOutputs(double[] inputs)
        {
            double[] nextInput = inputs;
            foreach(var networkLayer in NetworkLayers)
            {
                networkLayer.CalculateOutput(nextInput);
                nextInput = networkLayer.Output;
            }
            return NetworkLayers[NetworkLayers.Count - 1].Output;
        }

        // Gets all weights as a one dimensional array
        public double[] GetFlattenWeights()
        {
            List<double> flattenWeights = new List<double>();
            for (int i = 0; i < NetworkLayers.Count; i++)
            {
                for (int j = 0; j < NetworkLayers[i].NrNeurons; j++)
                {
                    for (int k = 0; k < NetworkLayers[i].NrInputs; k++)
                    {
                        flattenWeights.Add(NetworkLayers[i].Weights[j, k]);
                    }
                    if (!NetworkLayers[i].IsOutput)
                    {
                        flattenWeights.Add(NetworkLayers[i].BiasWeights[j]);
                    }
                }
            }
            return flattenWeights.ToArray();
        }

        // Sets all weights based on one dimensional array
        public void SetWeights(double[] weights)
        {
            int counter = 0;
            for (int i = 0; i < NetworkLayers.Count; i++)
            {
                for (int j = 0; j < NetworkLayers[i].NrNeurons; j++)
                {
                    for (int k = 0; k < NetworkLayers[i].NrInputs; k++)
                    {
                        NetworkLayers[i].Weights[j, k] = weights[counter];
                        counter++;
                    }
                    if (!NetworkLayers[i].IsOutput)
                    {
                        NetworkLayers[i].BiasWeights[j] = weights[counter];
                        counter++;
                    }
                }
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < NetworkLayers.Count; i++)
            {
                sb.AppendLine("Layer " + i);
                for(int j = 0; j < NetworkLayers[i].NrNeurons; j++)
                {
                    for(int k = 0; k < NetworkLayers[i].NrInputs; k++)
                    {
                        sb.Append(NetworkLayers[i].Weights[j, k] + " ");
                    }
                    sb.AppendLine();
                }

            }
            return sb.ToString();
        }

        public string WeightsAsString()
        {
            StringBuilder sb = new StringBuilder();
            List<double> flattenWeights = new List<double>();
            for (int i = 0; i < NetworkLayers.Count; i++)
            {
                for (int j = 0; j < NetworkLayers[i].NrNeurons; j++)
                {
                    for (int k = 0; k < NetworkLayers[i].NrInputs; k++)
                    {
                        sb.Append(NetworkLayers[i].Weights[j, k]);
                        sb.Append(";");
                    }
                    if (!NetworkLayers[i].IsOutput)
                    {
                        sb.Append(NetworkLayers[i].BiasWeights[j]);
                        sb.Append(";");
                    }
                }
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

        // Not quite sure which one of these I use
        //public int CompareTo(object obj)
        //{
        //    if (obj is Network)
        //    {
        //        return Fitness.CompareTo((obj as Network).Fitness);
        //    }
        //    throw new ArgumentException("Object is not a Network");
        //}

        //public int CompareTo(Network other)
        //{
        //    if (Fitness == other.Fitness)
        //    {
        //        return 0;
        //    }
        //    return Fitness > other.Fitness ? -1 : 1;
        //}

        //public int Compare(object x, object y)
        //{
        //    if (x is Network && y is Network)
        //    {
        //        return (x as Network).CompareTo(y as Network);
        //    }
        //    return 0;
        //}
    }
}

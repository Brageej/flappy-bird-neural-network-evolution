﻿using NeuralNetwork.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork
{
    public class NetworkLayer
    {
        public double[,] Weights { get; set; }
        public double[] BiasWeights { get; set; }
        public double[] Output { get; set; }
        public int NrNeurons { get; set; }
        public int NrInputs { get; set; }

        public bool IsOutput { get; set; }

        public IActivationFunction ActivationFunction { get; set; }

        public NetworkLayer(int nrNeurons, int nrInputs, IActivationFunction activationFunction, bool isOutput, Random random)
        {
            NrNeurons = nrNeurons;
            NrInputs = nrInputs;
            ActivationFunction = activationFunction;
            IsOutput = isOutput;
            IntitializeWeights(random);
            InitializeBiasWeights(random);
        }

        public void IntitializeWeights(Random random)
        {
            Weights = new double[NrNeurons, NrInputs];
            for (int i= 0; i < NrNeurons; i++)
            {
                for(int j = 0; j < NrInputs; j++)
                {             
                    double weight = random.NextGaussian(0, 1);
                    Weights[i, j] = weight;
                }
            }
        }

        public void InitializeBiasWeights(Random random)
        {
            if (!IsOutput)
            {
                BiasWeights = new double[NrNeurons];
                for (int i = 0; i < NrNeurons; i++)
                {
                    double weight = random.NextGaussian(0, 1);
                    BiasWeights[i] = weight;
                }
            }
        }

        public void CalculateOutput(double[] input)
        {
            Output = new double[NrNeurons];
            double[] weightedSums = new double[NrNeurons];
            for (int i = 0; i < NrNeurons; i++)
            {
                for(int j = 0; j < NrInputs; j++)
                {
                    weightedSums[i] += input[j] * Weights[i, j]; 
                }
                if (!IsOutput)
                {
                    weightedSums[i] += 1 * BiasWeights[i];
                }
            }
            Output = ActivationFunction.CalculateOutput(weightedSums);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.Evolution
{
    public class Individual : IComparable, IComparable<Individual>, IComparer
    {
        public Network Network { get; set; }
        public double Fitness { get; set; }

        public Individual(Network network)
        {
            Network = network;
        }

        public int CompareTo(object obj)
        {
            if (obj is Individual)
            {
                return Fitness.CompareTo((obj as Individual).Fitness);
            }
            throw new ArgumentException("Object is not a Network");
        }

        public int CompareTo(Individual other)
        {
            if (Fitness == other.Fitness)
            {
                return 0;
            }
            return Fitness > other.Fitness ? -1 : 1;
        }

        public int Compare(object x, object y)
        {
            if (x is Individual && y is Individual)
            {
                return (x as Individual).CompareTo(y as Individual);
            }
            return 0;
        }
    }
}

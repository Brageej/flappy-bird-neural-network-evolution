﻿using NeuralNetwork.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNetwork.Evolution
{
    public class GenerationCreator
    {

        public Individual[] CreateGeneration(Individual[] prevGen, double elitism, double crossoverRate, double mutationRate)
        {
            List<Individual> nextGen = new List<Individual>();
            int toTake = Convert.ToInt32(prevGen.Length * elitism);
            HandleElitism(nextGen, prevGen.Take(toTake).Where(x => x.Fitness > 0).ToArray());
            Breed(nextGen, prevGen, crossoverRate, mutationRate);
            return nextGen.ToArray();
        }

        public void HandleElitism(List<Individual> nextGen, Individual[] prevGen)
        {
            nextGen.AddRange(prevGen);
        }

        public void Breed(List<Individual> nextGen, Individual[] prevGen, double crossoverRate, double mutationRate)
        {
            Random random = new Random();
            double accumulativeFitness = 0;
            double[] rouletteWheel = CreateRouletteWheel(prevGen, accumulativeFitness);
            // We have a score of 0. Start with randoms again.
            if (accumulativeFitness == 0)
            {
                Network network = null;
                while(nextGen.Count < prevGen.Length)
                {
                    network = new Network(prevGen[0].Network.Props, random);
                    nextGen.Add(new Individual(network));
                }
            }
            while(nextGen.Count < prevGen.Length)
            {
                var parent = SpinnWheel(rouletteWheel, accumulativeFitness, random);
                var parent2 = SpinnWheel(rouletteWheel, accumulativeFitness, random, parent);
                List<double[]> offspring = Crossover(new List<double[]>() { prevGen[parent].Network.GetFlattenWeights(), prevGen[parent2].Network.GetFlattenWeights() }, random, crossoverRate);
                foreach (var childWeights in offspring)
                {
                    Network network = null;
                    network = new Network(prevGen[parent].Network.Props, random);
                    if (random.NextDouble() <= mutationRate)
                    {
                        network.SetWeights(Mutation(childWeights, random));
                    }
                    else
                    {
                        network.SetWeights(childWeights);
                    }
                    nextGen.Add(new Individual(network));
                }
            }
        }

        private int SpinnWheel(double[] rouletteWheel, double accumulativeFitness, Random random, int parent = -1)
        {
            double ball = random.NextDouble() * accumulativeFitness;
            int selectedParent = -1;
            while(selectedParent == -1)
            {
                for (int i = 0; i < rouletteWheel.Length; i++)
                {
                    if (rouletteWheel[i] > ball && parent != -1 && i != parent)
                    {
                        selectedParent = i;
                    }
                    else if (rouletteWheel[i] > ball && parent == -1)
                    {
                        selectedParent = i;
                    }
                }
            }
            return selectedParent;
        }

        public double[] CreateRouletteWheel(Individual[] prevGen, double accumulativeFitness)
        {
            double[] rouletteWheel = new double[prevGen.Length];

            for (int i = 0; i < prevGen.Length; i++)
            {
                accumulativeFitness += prevGen[i].Fitness;
                rouletteWheel[i] = accumulativeFitness;
            }
            return rouletteWheel;
        }

        public List<double[]> Crossover(List<double[]> parents, Random random, double crossoverRate)
        {
            int crossoverPoint = random.Next(0, parents[0].Length);
            List<double[]> offspring = new List<double[]>();
            if (random.NextDouble() <= crossoverRate)
            {
                List<double> child1 = new List<double>();
                child1.AddRange(parents[0].Take(crossoverPoint));
                child1.AddRange(parents[1].Skip(crossoverPoint));
                offspring.Add(child1.ToArray());

                List<double> child2 = new List<double>();
                child2.AddRange(parents[1].Take(crossoverPoint));
                child2.AddRange(parents[0].Skip(crossoverPoint));
                offspring.Add(child2.ToArray());
            }
            else
            {
                offspring = parents;
            }
            return offspring;
        }

        public double[] Mutation(double[] individual, Random random)
        {
            int mutationPoint = random.Next(0, individual.Length);
            List<double> child = new List<double>();
            child.AddRange(individual);
            double newValue = random.NextGaussian(0, 3);
            child[mutationPoint] = newValue;
            return child.ToArray();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.Evolution
{
    public class Population
    {
        public Individual[] Individuals;

        public Population(int populationSize, Properties props)
        {
            InitPopulation(populationSize, props);
        }

        private void InitPopulation(int populationSize, Properties props)
        {
            Individuals = new Individual[populationSize];
            var random = new Random();
            for (int i = 0; i < Individuals.Length; i++)
            {
                Individuals[i] = new Individual(new Network(props, random)); 
            }
        }
    }
}

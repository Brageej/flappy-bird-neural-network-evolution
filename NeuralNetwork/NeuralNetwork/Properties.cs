﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork
{
    public class Properties
    {
        public int InputSize { get; set; }
        public int OutputSize { get; set; }
        public int NrHiddenLayers { get; set; }
        public int NrNeuronsInLayer { get; set; }
        public IActivationFunction HiddenLayerActivationFunction { get; set; }
        public IActivationFunction OutputLayerActivationFunction { get; set; }
    }
}

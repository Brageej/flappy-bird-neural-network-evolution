﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork
{
    public interface IActivationFunction
    {
        double[] CalculateOutput(double[] weigthedSums);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.Extensions
{
    public static class PropertiesExtensions
    {
        public static bool Compatible(this Properties properties, Properties other)
        {
            var inputSize = properties.InputSize == other.InputSize;
            var hiddenLayers = properties.NrHiddenLayers == other.NrHiddenLayers;
            var hiddenNodes = properties.NrNeuronsInLayer == other.NrNeuronsInLayer;
            var output = properties.InputSize == other.InputSize;
            var hiddenActivation = properties.HiddenLayerActivationFunction == other.HiddenLayerActivationFunction;
            var outputActiviation = properties.OutputLayerActivationFunction == other.OutputLayerActivationFunction;
            return inputSize && hiddenLayers && hiddenNodes && output && hiddenActivation && outputActiviation;
        }
    }
}

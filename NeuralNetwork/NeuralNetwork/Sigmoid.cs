﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork
{
    public class Sigmoid : IActivationFunction
    {
        public double[] CalculateOutput(double[] weigthedSums)
        {
            int length = weigthedSums.Length;
            double[] output = new double[length];
            for(int i = 0; i < length; i++)
            {
                output[i] = 1 / (1 + Math.Exp(-weigthedSums[i]));
            }
            return output;
        }
    }
}

﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NeuralNetwork.Evolution;
using FlappyBird.Enums;

namespace FlappyBird.Sprites
{
    public class Bird : AnimatedSprite, ICollideable
    {
        public Vector2 Velocity;

        private int ThrustY = 300; // was 150

        private int Gravity = 15;

        private int Speed = 250;

        public float TotalDistanceTravelled = 0;

        public double Score = 0;

        public int ButtonPressedFrames = 0;

        public Sprite NextPipe = null;

        public Sprite NextTopPipe = null;

        public double NextHoleHeight;

        public double NextTopHoleHeight;

        public double DistanceToGround;

        public double NextHoleDistance;

        public double IsPipeInFront;

        public bool Crashed;

        public int FlapCounter = 0;

        public bool CanFlap = true;

        public int IndividualNr;

        private int ScreenHeight;

        private int HoleHeightNormalizer = 1080;
        private int HoleDistanceNormalizer = 800;
        private int VelocityNormalizer = 500;

        private FlappyBird FlappyBird;
    

        public Individual Individual;
        public Bird(FlappyBird flappyBird, Vector2 position, int width, int height, int individualNr, int screenHeight, Individual individual = null) : base(position, width, height, 2)
        {
            Texture = flappyBird.Content.Load<Texture2D>("FlappyBirdAnim");
            FlappyBird = flappyBird;
            Individual = individual;
            IndividualNr = individualNr;
            AddAnimation();
            ScreenHeight = screenHeight;
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Crashed)
            {
                spriteBatch.Draw(Texture, Rectangle, rectangles[FrameIndex],  Color.White);
            }
        }

        public override void Update(float time)
        {
            TotalDistanceTravelled += Speed * time;
            UpdateAnimation(time);
            if (Position.Y >= ScreenHeight || Position.Y <= 0)
            {
                Crashed = true;
            }
            if (FlapCounter >= 0)
            {
                CanFlap = false;
            }
            if (!CanFlap && FlapCounter == 0)
            {
                CanFlap = true;
            }
            if (!Crashed)
            {
                var shouldFlap = ShouldFlap();
                if (shouldFlap && CanFlap)
                {
                    Velocity.Y = -ThrustY;
                    FlapCounter = 0;
                }
                else
                {
                    FlapCounter--;
                }

                Velocity.Y += Gravity;
                position += Velocity * time;
            }
        }

        public override void ApplyScale(double scaleX, double scaleY)
        {
            ThrustY = (int)Math.Round(ThrustY * scaleY, 0);
            Gravity = (int)Math.Round(Gravity * scaleY, 0);
            HoleHeightNormalizer = (int)Math.Round(HoleHeightNormalizer * scaleY, 0);
            HoleDistanceNormalizer = (int)Math.Round(HoleDistanceNormalizer * scaleX, 0);
            VelocityNormalizer = (int)Math.Round(VelocityNormalizer * scaleY, 0);
        }

        private bool ShouldFlap()
        {
            if(FlappyBird.GameState == GameState.Game)
            {
                KeyboardState newKeyboardState = Keyboard.GetState();
                if (newKeyboardState.IsKeyDown(Keys.Space) && ButtonPressedFrames <= 2)
                {
                    ButtonPressedFrames += 1;
                    return true;
                }
                if (newKeyboardState.IsKeyUp(Keys.Space))
                {
                    ButtonPressedFrames = 0;
                    return false;
                }
                return false;
            }
            var random = new Random();
            if (NextPipe != null)
            {
                NextHoleHeight = ((Position.Y + Width/2) - (NextPipe.Position.Y)) / HoleHeightNormalizer;
                NextTopHoleHeight = ((Position.Y - Width / 2) - (NextTopPipe.Height)) / HoleHeightNormalizer;
                NextHoleDistance = (NextPipe.Position.X - (Position.X + Width)) / HoleDistanceNormalizer;
            }
            var inputs = new double[4] { NextHoleHeight, NextTopHoleHeight, NextHoleDistance, Velocity.Y / VelocityNormalizer };
            var outputs = Individual.Network.CalculateOutputs(inputs);
            return outputs[0] > 0;
            
        }

        public bool DoOverlap(Sprite other)
        {    
            return Rectangle.Intersects(other.Rectangle);
        }

        public void IncrementScore()
        {
            Score += 10;
        }
    }
}

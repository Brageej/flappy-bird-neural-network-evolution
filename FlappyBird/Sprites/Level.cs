﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NeuralNetwork.Evolution;
using FlappyBird.Enums;

namespace FlappyBird.Sprites
{
    public class Level
    {
        public Pipe[] Pipes;

        public Bird[] Birds;

        public Ground[] GroundTiles;

        public Background[] Backgrounds;

        public int BirdPosX = 960;

        public int BirdPosY = 560;

        public int BirdWidth = 40;

        public int BirdHeight = 40;

        private FlappyBird FlappyBird;

        private int PipeGapY = 175;

        private int PipeGapX = 800;

        private int PipeWidth = 100;

        private int GroundWidth = 520;

        private int GroundHeight = 130;

        private int StartPosX = 1760;

        private int StartPosY = 540;

        private int MaxPosY = 700;

        private int MinPosY = 380;

        public int ScreenHeight;

        private int ScreenWidth;

        private double ScaleFactorX = 1;

        private double ScaleFactorY = 1;

        private Pipe LastPipe;

        private Ground LastGround;

        private SpriteFont GameFont;

        private int PipesPerSimulation;

        public int PipesPassed = 0;

        public Level(FlappyBird flappyBird, int pipesPerSimulation, Individual[] individuals = null)
        {
            FlappyBird = flappyBird;
            ScreenHeight = flappyBird.GraphicsDevice.Viewport.Height;
            ScreenWidth = flappyBird.GraphicsDevice.Viewport.Width;
            ScaleFactorX = (double)ScreenWidth/1920;
            ScaleFactorY = (double)ScreenHeight/1080;
            ApplyScale();
            PipesPerSimulation = pipesPerSimulation;
            InitGround();
            InitBackground();
            InitPipes();
            InitBirds(1, individuals);
            ApplySpriteScale();
            GameFont = FlappyBird.Content.Load<SpriteFont>("GameFont");
        }

        public void ApplyScale ()
        {
            BirdPosX = (int)Math.Round(BirdPosX * ScaleFactorX, 0);
            BirdPosY = (int)Math.Round(BirdPosY * ScaleFactorY, 0);

            BirdWidth = (int)Math.Round(BirdWidth * ScaleFactorX, 0);
            BirdHeight = (int)Math.Round(BirdHeight * ScaleFactorY, 0);

            PipeGapX = (int)Math.Round(PipeGapX * ScaleFactorX, 0);
            PipeGapY = (int)Math.Round(PipeGapY * ScaleFactorY, 0);
            PipeWidth = (int)Math.Round(PipeWidth * ScaleFactorX, 0);

            GroundWidth = (int)Math.Round(GroundWidth * ScaleFactorX, 0);
            GroundHeight = (int)Math.Round(GroundHeight * ScaleFactorY, 0);

            StartPosX = (int)Math.Round(StartPosX * ScaleFactorX, 0);
            StartPosY = (int)Math.Round(StartPosY * ScaleFactorY, 0);

            MaxPosY = (int)Math.Round(MaxPosY * ScaleFactorY, 0);
            MinPosY = (int)Math.Round(MinPosY * ScaleFactorY, 0);
        }

        public void InitBackground()
        {
            Backgrounds = new Background[5];
            for(int i = 0; i < Backgrounds.Length; i++)
            {
                Backgrounds[i] = new Background(FlappyBird, new Vector2(ScreenWidth*2*i, 0), ScreenWidth*2, ScreenHeight);
            }
        }
        public void ApplySpriteScale()
        {
            foreach (var pipe in Pipes)
            {
                pipe.ApplyScale(ScaleFactorX,ScaleFactorY);
            }
            foreach (var bird in Birds)
            {
                bird.ApplyScale(ScaleFactorX, ScaleFactorY);
            }
            for (int i = 0; i < GroundTiles.Length; i++)
            {
                GroundTiles[i].ApplyScale(ScaleFactorX, ScaleFactorY);
            }
        }

        public void InitGround()
        {
            int nrGroundTiles = (int)Math.Round((double)(ScreenWidth / GroundWidth), 0) + 3;
            GroundTiles = new Ground[nrGroundTiles];
            for (int i = 0; i < nrGroundTiles; i++)
            {
                GroundTiles[i] = new Ground(FlappyBird, new Vector2(GroundWidth * i - (float)((5 * ScaleFactorX) * i), ScreenHeight - GroundHeight), GroundWidth, GroundHeight);
            }
            LastGround = GroundTiles[nrGroundTiles - 1];
        }

        public void InitBirds(int numberBirds, Individual[] individuals = null)
        {
            numberBirds = individuals != null ? individuals.Length : numberBirds;
            Birds = new Bird[numberBirds];
            for (int i = 0; i < numberBirds; i++)
            {
                Birds[i] = new Bird(FlappyBird, new Vector2(BirdPosX, BirdPosY), BirdWidth, BirdHeight, i, ScreenHeight, individuals != null ? individuals[i] : null);
                Birds[i].NextPipe = Pipes[0];
                Birds[i].NextTopPipe = Pipes[1];
            }
        }

        private void InitPipes()
        {
            Pipes = new Pipe[10];
            Pipe prevPipe = null;
            var random = new Random();
            for(int i = 0; i < Pipes.Length; i = i + 2)
            {
                if (prevPipe != null)
                {
                    int prevPosY = (int)prevPipe.Position.Y;
                    int maxValue = ScreenHeight - prevPosY;
                    int minValue = ScreenHeight - maxValue;
                    int posY = Math.Max(Math.Min(MaxPosY, prevPosY + random.Next(-minValue, maxValue)), MinPosY);
                    Pipes[i] = new Pipe(FlappyBird, new Vector2(prevPipe.Position.X + PipeGapX, posY), PipeWidth, ScreenHeight - posY, true);
                    Pipes[i + 1] = new Pipe(FlappyBird, new Vector2(prevPipe.Position.X + PipeGapX, 0), PipeWidth, posY - PipeGapY, false);
                }
                else
                {
                    Pipes[i] = new Pipe(FlappyBird, new Vector2(StartPosX + PipeGapX * i, StartPosY), PipeWidth, ScreenHeight - StartPosY, true);
                    Pipes[i + 1] = new Pipe(FlappyBird, new Vector2(StartPosX + PipeGapX * i, 0), PipeWidth, StartPosY - PipeGapY, false);
                }
                prevPipe = Pipes[i];
            }
            LastPipe = Pipes[8];
        }

        public void Update(float time)
        {
            HandleBirdPipeInteraction();
            for(int i = 0; i < Backgrounds.Length; i++)
            {
                Backgrounds[i].Update(time);
            }
            foreach(var pipe in Pipes)
            {
                pipe.Update(time);
            }
            var crashedBirds = 0;
            for(int i = 0; i < Birds.Length; i++)
            {
                if (!Birds[i].Crashed)
                {
                    Birds[i].Update(time);
                }
                else
                {
                    crashedBirds++;
                }
            }
            if (crashedBirds == Birds.Length)
            {
                FlappyBird.GameState = GameState.Evolution;
            }
            for(int i = 0; i < GroundTiles.Length; i++)
            {
                if (GroundTiles[i].Position.X + GroundWidth <= 0)
                {
                    GroundTiles[i].ResetGround(new Vector2(LastGround.Position.X + GroundWidth-5, ScreenHeight - GroundHeight));
                    LastGround = GroundTiles[i];
                }
                GroundTiles[i].Update(time);
            }
        }

        public void HandleBirdPipeInteraction()
        {
            if(PipesPassed == PipesPerSimulation)
            {
                for (int j = 0; j < Birds.Length; j++)
                {
                    if (!Birds[j].Crashed)
                    {
                        Birds[j].Score = Birds[j].TotalDistanceTravelled - Math.Abs((Birds[j].NextHoleHeight - (PipeGapY / 2)) - Birds[j].Position.Y);
                    }
                }
                FlappyBird.GameState = GameState.Evolution;
            }
            var random = new Random();
            for (int i = 0; i < Pipes.Length; i = i + 2)
            {
                var currentPipe = Pipes[i];
                var birdPositionX = Birds[0].Position.X;
                var pipePosition = currentPipe.Position.X;
                if (currentPipe.Position.X + PipeWidth <= 0)
                {
                    int prevPosY = (int)LastPipe.Position.Y;
                    int maxValue = ScreenHeight - prevPosY;
                    int minValue = ScreenHeight - maxValue;
                    int posY = Math.Max(Math.Min(MaxPosY, prevPosY + random.Next(-minValue, maxValue)), MinPosY);
                    Pipes[i] = new Pipe(FlappyBird, new Vector2(LastPipe.Position.X + PipeGapX, posY), PipeWidth, ScreenHeight - posY, true);
                    Pipes[i + 1] = new Pipe(FlappyBird, new Vector2(LastPipe.Position.X + PipeGapX, 0), PipeWidth, posY - PipeGapY, false);
                    Pipes[i].ApplyScale(ScaleFactorX, ScaleFactorY);
                    Pipes[i + 1].ApplyScale(ScaleFactorX, ScaleFactorY);
                    LastPipe = Pipes[i];
                }
                else if ((birdPositionX >= currentPipe.Position.X) && (birdPositionX <= (currentPipe.Position.X + PipeWidth)))
                {
                    for(int j = 0; j < Birds.Length; j++)
                    {
                        DetectCollision(Birds[j], Pipes[i]);
                        DetectCollision(Birds[j], Pipes[i + 1]);
                    }
                }
                if (!currentPipe.Passed && currentPipe.Position.X + PipeWidth < birdPositionX)
                {
                    currentPipe.TogglePassed();
                    PipesPassed++;
                    for (int j = 0; j < Birds.Length; j++)
                    {
                        if(!Birds[j].Crashed)
                        {
                            Birds[j].IncrementScore();
                        }
                        Birds[j].NextPipe = Pipes[(i + 2) % 10];
                        Birds[j].NextTopPipe = Pipes[(i + 3) % 10];
                    }
                }
            }
        }

        public void DetectCollision(Bird bird, Sprite pipe)
        {
            var collides = bird.DoOverlap(pipe);
            if (collides)
            {
                bird.Crashed = true;
                bird.Score = bird.TotalDistanceTravelled - Math.Abs((bird.NextHoleHeight - (PipeGapY/2)) - bird.Position.Y);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Backgrounds.Length; i++)
            {
                Backgrounds[i].Draw(spriteBatch);
            }
            foreach (var pipe in Pipes)
            {
                pipe.Draw(spriteBatch);
            }
            foreach (var bird in Birds)
            {
                bird.Draw(spriteBatch);
            }
            for (int i = 0; i < GroundTiles.Length; i++)
            {
                GroundTiles[i].Draw(spriteBatch);
            }
            if (FlappyBird.GameState == GameState.Simulation)
            {
                spriteBatch.DrawString(GameFont, "Generation: " + FlappyBird.CurrentGeneration, new Vector2(50, 50), Color.White);
            }
            else if (FlappyBird.GameState == GameState.Game || FlappyBird.GameState == GameState.Presentation)
            {
                spriteBatch.DrawString(GameFont, "Score: " + (Birds[0].TotalDistanceTravelled - Math.Abs((Birds[0].NextHoleHeight - (PipeGapY / 2)) - Birds[0].Position.Y)), new Vector2(50, 50), Color.White);
            }
        }
    }
}

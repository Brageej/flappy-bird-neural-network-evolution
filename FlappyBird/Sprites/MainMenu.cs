﻿using System;
using FlappyBird.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FlappyBird.Sprites
{
    public class MainMenu
    {
        private Button SimulationButton { get; set; }
        private Button PresentationButton { get; set; }
        private int SimulationButtonPosX = 960;
        private int SimulationButtonPosY = 540;
        private Button StartGameButton { get; set; }
        private double ScaleFactorX { get; set; }
        private double ScaleFactorY { get; set; }
        public MainMenu(FlappyBird flappyBird)
        {
            var screenHeight = flappyBird.GraphicsDevice.Viewport.Height;
            var screenWidth = flappyBird.GraphicsDevice.Viewport.Width;
            ScaleFactorX = (double)screenWidth / 1920;
            ScaleFactorY = (double)screenHeight / 1080;
            ApplyScale(ScaleFactorX, ScaleFactorY);
            SimulationButton = new Button("Start Bird Evolution (E)", GameState.InitSimulation, flappyBird, Keys.E, new Vector2(50, 50));
            PresentationButton = new Button("Start Fittest Bird (F)", GameState.InitPresentation, flappyBird, Keys.F, new Vector2(50, 120));
            StartGameButton = new Button("Start Game (G)", GameState.InitGame, flappyBird, Keys.G, new Vector2(50, 190));
        }
        public void ApplyScale(double scaleFactorX, double scaleFactorY)
        {
            SimulationButtonPosX = (int)Math.Round(SimulationButtonPosX * ScaleFactorX, 0);
            SimulationButtonPosY = (int)Math.Round(SimulationButtonPosY * ScaleFactorY, 0);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            SimulationButton.Draw(spriteBatch);
            PresentationButton.Draw(spriteBatch);
            StartGameButton.Draw(spriteBatch);
        }

        public void Update(GameTime gameTime, float ScaledTime)
        {
            var time = (float)gameTime.ElapsedGameTime.TotalSeconds * ScaledTime;
            SimulationButton.Update(time);
            PresentationButton.Update(time);
            StartGameButton.Update(time);
        }
    }
}

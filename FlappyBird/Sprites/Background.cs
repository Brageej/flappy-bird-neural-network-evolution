﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBird.Sprites
{
    public class Background : Sprite
    {
        public int Speed = 50;

        public int ScreenWidth;

        public int ScreenHeight;

        public Background(FlappyBird flappyBird, Vector2 position, int width, int height) : base(position, width, height)
        {
            ScreenWidth = flappyBird.GraphicsDevice.Viewport.Width;
            ScreenHeight = flappyBird.GraphicsDevice.Viewport.Height;
            Texture = flappyBird.Content.Load<Texture2D>("FlappyBirdBackground");
        }
        public override void ApplyScale(double scaleFactorX, double scaleFactorY)
        {
            Speed = (int)Math.Round(Speed * scaleFactorX, 0);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Rectangle, Color.White);
        }

        public override void Update(float time)
        {
            position.X -= Speed * time;
        }
    }
}

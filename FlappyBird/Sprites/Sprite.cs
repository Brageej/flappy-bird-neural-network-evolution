﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlappyBird.Sprites
{
    public abstract class Sprite
    {
        public Texture2D Texture { get; set; }
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        protected Vector2 position;

        public int Width { get; set; }
        public int Height { get; set; }

        public bool Stopped { get; set; }

        public Rectangle Rectangle { get { return new Rectangle((int)position.X, (int)position.Y, Width, Height); } }

        public Sprite(Vector2 position, int width, int height)
        {
            this.position = position;
            Width = width;
            Height = height;
        }

        public void Stop()
        {
            Stopped = true;
        }

        public abstract void Update(float gameTime);

        public abstract void Draw(SpriteBatch spriteBatch);

        public abstract void ApplyScale(double scaleFactorX, double scaleFactorY);

    }
}

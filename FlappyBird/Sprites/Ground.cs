﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBird.Sprites
{
    public class Ground : Sprite
    {
        public int Speed = 250;
        public Ground(FlappyBird flappyBird, Vector2 position, int width, int height) : base(position, width, height)
        {
            Texture = flappyBird.Content.Load<Texture2D>("GroundTile2");
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Rectangle, Color.White);
        }

        public override void Update(float time)
        {
            position.X -= Speed * time;
        }

        public void ResetGround(Vector2 newPosition)
        {
            position = newPosition;
        }

        public override void ApplyScale(double scaleFactorX, double scaleFactorY)
        {
            Speed = (int)Math.Round(Speed * scaleFactorX, 0);
        }
    }
}

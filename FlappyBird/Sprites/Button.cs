﻿using FlappyBird.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlappyBird.Sprites
{
    public class Button
    {
        private string ButtonText { get; set; }
        private GameState ButtonPressedState { get; set; }
        private Keys Key { get; set; }
        private SpriteFont GameFont { get; set; }
        private FlappyBird FlappyBird { get; set; }
        private MouseState OldMouseState { get; set; }
        private Vector2 Position { get; set; }
        public Button(string buttonText, GameState buttonPressedState, FlappyBird flappyBird, Keys key, Vector2 position)
        {
            ButtonText = buttonText;
            ButtonPressedState = buttonPressedState;
            FlappyBird = flappyBird;
            Key = key;
            Position = position;
            //Texture = flappyBird.Content.Load<Texture2D>("Pipe");
            GameFont = flappyBird.Content.Load<SpriteFont>("GameFont");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(Texture, Rectangle, Color.White);
            spriteBatch.DrawString(GameFont, ButtonText, Position, Color.White);
        }

        public void Update(float gameTime)
        {
            KeyboardState newKeyboardState = Keyboard.GetState();
            if (newKeyboardState.IsKeyDown(Key))
            {
                FlappyBird.GameState = ButtonPressedState;
            }
            //MouseState mouseState = Mouse.GetState();
            //int x = mouseState.X;
            //int y = mouseState.Y;
            //Mouse.SetPosition(x, y);
            //if (Rectangle.Intersects(new Rectangle(x, y, 5, 5)))
            //{
            //    if (OldMouseState != null && OldMouseState.LeftButton == ButtonState.Released && mouseState.LeftButton == ButtonState.Pressed)
            //    {
            //        FlappyBird.GameState = ButtonPressedState;
            //    }
            //}
            //OldMouseState = mouseState;
        }
    }
}

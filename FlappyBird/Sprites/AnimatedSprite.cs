﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlappyBird.Sprites
{
    public abstract class AnimatedSprite : Sprite
    {
        public Rectangle[] rectangles;

        public int Frames;

        public int FrameIndex;

        public int ticksElapsed;

        public int ticsBetweenUpdates = 5;
        public AnimatedSprite(Vector2 position, int width, int height, int frames) : base(position, width, height)
        {
            Frames = frames;
        }

        public void AddAnimation()
        {
            int width = Texture.Width / Frames;
            rectangles = new Rectangle[Frames];
            for (int i = 0; i < rectangles.Length; i++)
            {
                rectangles[i] = new Rectangle(i * width, 0, width, Texture.Height);
            }
        }

        public void UpdateAnimation(float time)
        {
            ticksElapsed++;
            if(ticksElapsed > ticsBetweenUpdates)
            {
                ticksElapsed -= ticsBetweenUpdates;
                if (FrameIndex < Frames - 1)
                {
                    FrameIndex++;
                }
                else
                {
                    FrameIndex = 0;
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBird.Sprites
{
    public class Pipe : Sprite
    {

        public int Speed = 250;

        public bool Passed = false;

        public bool Bottom;

        public Pipe(FlappyBird flappyBird, Vector2 position, int width, int height, bool bottom) : base(position, width, height)
        {
            var textureName = "TreeTrunk" + (bottom ? "" : "2");
            Texture = flappyBird.Content.Load<Texture2D>(textureName);
            Bottom = bottom;
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if(Bottom)
            {
                spriteBatch.Draw(Texture, Rectangle, new Rectangle(0, 0, Width, Height), Color.White);
            }
            else
            {
                spriteBatch.Draw(Texture, Rectangle, new Rectangle(0, Texture.Height-Height, Width, Height), Color.White);
            }
        }

        public override void Update(float time)
        {
            if (!Stopped)
            {
                position.X -= Speed * time;
            }
        }

        public void TogglePassed()
        {
            Passed = Passed ? false : true;
        }

        public override void ApplyScale(double scaleFactorX, double scaleFactorY)
        {
            Speed = (int)Math.Round(Speed * scaleFactorX, 0);
        }
    }
}

﻿using FlappyBird.Repository.Model;
using NeuralNetwork;
using NeuralNetwork.Evolution;
using NeuralNetwork.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlappyBird.Repository
{
    public class PreTrainedBirdRepository
    {
        private static readonly string ResultsFolder = @"C:\Workspace\FlappyBirdNeuralNetwork\Results\";
        public static void StoreBestIndividual(Individual[] individuals)
        {
            var bestIndividual = individuals[0];
            var prevBestIndividual = FetchedTrainedBirdItems().OrderByDescending(x => x.Fitness).FirstOrDefault();
            if (prevBestIndividual != null)
            {
                if (bestIndividual.Fitness > prevBestIndividual.Fitness)
                {
                    StoreIndividual(bestIndividual);
                }
            }
            else
            {
                StoreIndividual(bestIndividual);
            }
        }

        private static void StoreIndividual(Individual individual)
        {
            var fileloc = ResultsFolder + "best2" + ".json";
            using (StreamWriter sw = new StreamWriter(fileloc))
            {
                TrainedBirdItem trainedBirdItem = new TrainedBirdItem()
                {
                    Weights = individual.Network.GetFlattenWeights(),
                    Fitness = individual.Fitness
                };
                var serializedTrainedBirdItem = JsonConvert.SerializeObject(trainedBirdItem);
                sw.WriteLine(serializedTrainedBirdItem);
            }
        }

        public static List<TrainedBirdItem> FetchedTrainedBirdItems()
        {
            var trainedBirdItems = new List<TrainedBirdItem>();
            var filenames = Directory.GetFiles(ResultsFolder, "best*", SearchOption.TopDirectoryOnly);
            foreach (var filename in filenames)
            {
                var serializedTrainedBirdItem = File.ReadAllText(filename);
                var trainedBirdItem = JsonConvert.DeserializeObject<TrainedBirdItem>(serializedTrainedBirdItem);
                trainedBirdItems.Add(trainedBirdItem);
            }
            return trainedBirdItems;
        }
    }
}

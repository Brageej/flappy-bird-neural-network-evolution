﻿using NeuralNetwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlappyBird.Repository.Model
{
    public class TrainedBirdItem
    {
        public double[] Weights { get; set; }
        public double Fitness { get; set; }
    }
}

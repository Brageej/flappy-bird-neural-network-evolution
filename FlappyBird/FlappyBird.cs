﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NeuralNetwork;
using NeuralNetwork.Evolution;
using System.Drawing;
using System.IO;
using FlappyBird.Sprites;
using System;
using System.Diagnostics;
using FlappyBird.Enums;
using Newtonsoft.Json;
using FlappyBird.Repository.Model;
using System.Collections.Generic;
using NeuralNetwork.Extensions;
using FlappyBird.Repository;
using System.Linq;

namespace FlappyBird
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class FlappyBird : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private readonly float ScaledTime = 3.0f;
        private Individual[] Individuals { get; set; }
        public Properties Properties { get; set; }
        private int PipesPerSimulation { get; set; }
        public GameState GameState = GameState.Menu;
        private int Best { get; set; }
        private int NrGenerations { get; set; }
        public int CurrentGeneration { get; set; }
        public Level Level;
        public MainMenu MainMenu;
        public string ResultsFolder = @"C:\Workspace\FlappyBirdNeuralNetwork\Results\";
        public FlappyBird()
        {
            graphics = new GraphicsDeviceManager(this);
            PipesPerSimulation = 20;
            CurrentGeneration = 1;
            IsFixedTimeStep = false;
            Content.RootDirectory = "Content";
            Properties = new Properties
            {
                InputSize = 4,
                OutputSize = 1,
                NrHiddenLayers = 1,
                NrNeuronsInLayer = 3,
                HiddenLayerActivationFunction = new Sigmoid(),
                OutputLayerActivationFunction = new Step(0.0)
            };
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Level = new Level(this, PipesPerSimulation, Individuals);
            MainMenu = new MainMenu(this);
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            switch (GameState)
            {
                case GameState.Simulation:
                    var time = (float)gameTime.ElapsedGameTime.TotalSeconds * ScaledTime;
                    Level.Update(time);
                    break;
                case GameState.Evolution:
                    if (CurrentGeneration >= NrGenerations)
                    {
                        GameState = GameState.Menu;
                        break;
                    }
                    CurrentGeneration++;
                    // Collecting fitness scores
                    var birds = Level.Birds;
                    foreach (var bird in birds)
                    {
                        Individuals[bird.IndividualNr].Fitness = bird.Score;
                    }
                    // Sorting to get best individuals first.
                    Array.Sort(Individuals);
                    // Storing best network from previous generation if higher fitness then currently highest fitness.
                    PreTrainedBirdRepository.StoreBestIndividual(Individuals);
                    // Breeding new generation
                    if (Level.PipesPassed == PipesPerSimulation)
                    {
                        GameState = GameState.Menu;
                        break;
                    }
                    else
                    {
                        GenerationCreator generationCreator = new GenerationCreator();
                        var newGen = generationCreator.CreateGeneration(Individuals, 0.10, 1, 0.8);
                        Individuals = newGen;
                        // Resetting Level
                        Level = new Level(this, PipesPerSimulation, Individuals);
                        GameState = GameState.Simulation;
                    }
                    break;
                case GameState.InitPresentation:
                    // Initializing individuals
                    Individuals = new Population(1, Properties).Individuals;
                    // Fetching stored individual from json file. (Neural Network weights)
                    var pretrained = PreTrainedBirdRepository.FetchedTrainedBirdItems().OrderByDescending(x => x.Fitness).ToList();
                    var weights = pretrained[0].Weights;
                    if (weights.Length > 0)
                    {
                        Individuals[0].Network.SetWeights(weights);
                    }
                    // Setting Level
                    Level = new Level(this, 1000, Individuals);
                    CurrentGeneration = 1;
                    NrGenerations = 1;
                    GameState = GameState.Presentation;
                    break;
                case GameState.Presentation:
                    var pTime = (float)gameTime.ElapsedGameTime.TotalSeconds * ScaledTime;
                    Level.Update(pTime);
                    break;
                case GameState.InitSimulation:
                    // Initializing individuals
                    Individuals = new Population(300, Properties).Individuals;
                    // Setting Level
                    Level = new Level(this, PipesPerSimulation, Individuals);
                    CurrentGeneration = 1;
                    NrGenerations = 100;
                    GameState = GameState.Simulation;
                    break;
                case GameState.Menu:
                    IsMouseVisible = true;
                    MainMenu.Update(gameTime, ScaledTime);
                    break;
                case GameState.InitGame:
                    Individuals = new Population(1, Properties).Individuals;
                    Level = new Level(this, PipesPerSimulation, Individuals);
                    CurrentGeneration = 1;
                    NrGenerations = 1;
                    GameState = GameState.Game;
                    break;
                case GameState.Game:
                    var gTime = (float)gameTime.ElapsedGameTime.TotalSeconds * ScaledTime;
                    Level.Update(gTime);
                    break;
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            Level.Draw(spriteBatch);
            if (GameState == GameState.Menu)
            {
                MainMenu.Draw(spriteBatch);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}

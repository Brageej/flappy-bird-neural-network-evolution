﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlappyBird.Enums
{
    public enum GameState
    {
        Game=0,
        Simulation=1,
        Presentation=2,
        Evolution=3,
        Menu=4,
        InitPresentation=5,
        InitSimulation=6,
        InitGame=7
    }
}
